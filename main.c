/*
// ��������� - "������������ ���������� ��� ������� �������"
// ������� - NOKIA 1100 , ������� ���������� ����������� SPI.
// � ���������� ������� ����������� ��������� ������� ���� �������� �������
// ���������� Atmega328p , �������� ������� 16���
// ������� ���������� ��� ADC - ������� �������� �� 3.3 ������. (�������� 3.27 �����, ��� ���� � ���� ���������) ����� ����� ������ �������!

  Encoder:
- PD7 -Encoder A      (arduino pin 7)
- PD6 -Encoder B      (arduino pin 6)
- PD5 -Button Encoder (arduino pin 5)

  LCD1602:
- PC5 -DATA7  LCD (arduino pin A5)
- PC4 -DATA6  LCD (arduino pin A4)
- PC3 -DATA5  LCD (arduino pin A3)
- PC2 -DATA4  LCD (arduino pin A2)
- PB4 -E      LCD (arduino pin 12)
- PB3 -RS     LCD (arduino pin 11)
- PB2 -RW     LCD (arduino pin 10)

  Inpit signal
- PD2 -input triger (arduino pin 2) (INT0)
- PC0 -input signal (arduino pin A0)(ADC0)
  
  Output signal
- PB1 -PWM (arduino pin 8)(OC1A)

  UART
-PD0 -RX   (arduino pin 0)
-PD1 -TX   (arduino pin 1)

*/
#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "adc.c"
#include "encoder.h"
#include "lcd.h"

#define F_CPU 16000000UL
#define SET(p, n)    p |= _BV(n)
#define CLR(p, n)    p &= ~_BV(n)
#define INV(p, n)    p ^= _BV(n)

/* ���������� ��� ����� ������ ������� */
#define DDR_Button  DDRD   
#define PORT_Button PORTD
#define PIN_Button  PIND
#define Button      5

/* ����������� ����� ����������� PWM timer1 (OC1A) */
#define DDR_PWM   DDRB 
#define PORT_PWM  PORTB 
#define PIN_PWM   0

/* ������ ��� � ������������*/
#define PWMperiod  5  

/*USART Config*/
#define baudrate 128000L //115200L
#define bauddivider ((F_CPU+baudrate*8)/(baudrate*16)-1)
#define HI(x) ((x)>>8)
#define LO(x) ((x)& 0xFF)

#define MAX_SAMPLES  300       // ������ ������

unsigned int   sample_index = 0;            // ����� ������ ������
unsigned char  zero[MAX_SAMPLES];         // ����� ��� ������ ������� ����� ������, ������ ��������� ��� �������
unsigned int   adc[MAX_SAMPLES];          // ����� ��� �������� ������ ��� X
unsigned char  OPTOsensor = 0;
/*
unsigned int eeprom_eemem_freguence EEMEM ;  
unsigned int eeprom_eemem_contrast EEMEM ;  
 //eeprom_write_word(&eeprom_eemem_freguence, 5799); // ������ � eeprom
 //eeprom_freg = eeprom_read_word(&eeprom_eemem_freguence); // ������ �� eeprom
*/

char *pBuf;                     // ��������� ���������� ��������� �� �����
unsigned int TimerCount_ms = 0; // ������� ���������� , �������� 65535 ��, ��� 65 ������ ..��������� ������
unsigned int PWMcount;          // ������� ������� ��� 
unsigned int PWM = 0;           // ���������� ��� �������� �������� ��� � �������������
unsigned int PWMmin = 1200;  // ����������� ���
unsigned int PWMmax = 1800;  // ������������ ���
unsigned char PWM_on= 0;     // ���������/���������� ��� 
unsigned int  Angle =0;      // ���� �������� ���� �������, ������ ��������
unsigned char pressing =0;   // ���� � ������� ������
unsigned char meny =1;       // ����� ����, ������� ������������� �� ������ ������ 
unsigned int  Thrt;          // ���������� ���������� �������� � ������
unsigned long int  Thrt_count;    // ������� ������� ������� � ������������
unsigned int  Thrt_null=0;   // � ������� ����� �������� �������� ��������� �� ������� ��������, ���� �� ��� �� Thrt = 0;
unsigned int  Thrt_dev=0;    // ������� ���������� ���������.
unsigned int  AngleCount=0;  // ��������� ������� ��� �������� ���� � ������������
unsigned char redyData = 0;  // ���� ���������� ������ ������

unsigned int timer_count_ms_read(void);
void timer_count_ms_write(unsigned int t);
void Timer1_init(void);
void Timer1_stop(void);
void Timer1_start(unsigned int count);

ISR(INT0_vect)//���������� INT0 (������� ����� ������)
{
OPTOsensor =1;
//unsigned long int u = (100000/Thrt_count)*60;  // ��������� ������� ������
//Thrt = u/100;
//Thrt_count = 0;

/*  ������� �������� ��������
Thrt_null =0; // �������� ��������� ���� ��������.
Thrt_dev++;
if(Thrt_dev==20)
{
Thrt_count = Thrt_count /20;
unsigned long int u = (1000/Thrt_count)*60;  // ��������� ������� ������
Thrt = u/1;
Thrt_count = 0;
Thrt_dev=0;
}
*/
//if(Angle){AngleCount = Angle;} else {OPTOsensor =1;} // ���� ���� �������� ����, �� ������� ��.
}


/*********** ��������� ������, ���������� ������ �����������**********/
/*********************************************************************/
/*********************************************************************/
SIGNAL(TIMER0_COMPA_vect) // ���������� ������ 1ms
{
EncoderScan(); // ��������� �������
PWMcount++;
if(PWMcount == PWMperiod)                 // ���� ������� �������� ������ ����, �� ���� ��������� ������ ����������
  { 
    PWMcount=0; 
    SET(PORT_PWM,PIN_PWM); 
    if(PWM_on) Timer1_start(PWM + PWMmin); else Timer1_start(PWMmin); // ���� ������ ������ �� ������ ����������� ���, ����� ����������� ���
  }
TimerCount_ms++;
Thrt_count++;
Thrt_null++;
if(Thrt_null > 200) Thrt=0;            // ���� ����� �� ���������, �� ������� = 0.
if(TimerCount_ms==65535) TimerCount_ms=0; // �������� ������� ��� ������������� 
 /* if(AngleCount)
   {
    AngleCount--;
    if(AngleCount==0) OPTOsensor =1;
   }*/
}

unsigned int timer_count_ms_read(void)
{
  return TimerCount_ms;
}

void timer_count_ms_write(unsigned int t)
{
  TimerCount_ms = t;
  return;
}


/* ������ ��� ������� ������������ �������� ���, ������ ���������� */
/*******************************************************************/
/*******************************************************************/

SIGNAL(TIMER1_COMPA_vect)
{
CLR(PORT_PWM,PIN_PWM); 
Timer1_stop();
}

SIGNAL(TIMER2_COMPA_vect)  // ���������� ������ 500���, � ��� ���������� ������.
{
if(sample_index)  // ���� ���� �� ���� ��������������
{
if(sample_index < MAX_SAMPLES)
  {
              
              adc[sample_index]= 1024 - ADC_read(0);      // ������ �������� ������������� ����� ��� (X)
              zero[sample_index]= OPTOsensor;        // ���������� ��������� ����������
              OPTOsensor=0;    
              sample_index++;         
  }        
  else 
  {
    sample_index=0; // ���� ���� ����� ���������, �� ���������� ���� ���������� ������
    redyData = 1;
  }
}
else
{
  if(OPTOsensor) sample_index =1; // ���� ������������� ������, �� �������� ��������
} 
}

void Timer1_init(void)  // ���������� ��� ������������ ��� �������
{
   //��������� ��������
    TIMSK1 = 0b00000010;              // ��������� ���������� �� ���������� �0 
    TCCR1A |= (0<<WGM10)|(0<<WGM11) ; // ����� ������
   // TCCR1B = (0<<WGM12)|(0<<WGM13) |(0<<CS12)|(1<<CS11)|(0<<CS10) ; // ��������� ������ fclk/8, ����� ������
    //TCCR1B = 0b00000010;            // �������� fclk/8, 16000000/8=2000000, 2000000/1000000��� = 2 �����/���                    
}

void Timer2_init(void) // ���������� ������ 500 ���, ��� ������ �������
{
   //��������� ��������
    TIMSK2 |= (1<<OCIE2A);            // ��������� ���������� �� ���������� �0 
    TCCR2A |= (0<<WGM20)|(0<<WGM21) ; // ����� ������

    TCCR2B = (0<<WGM22) |(0<<CS22)|(1<<CS21)|(1<<CS20) ; // ��������� ������ fclk/8                   
}

void Timer1_stop(void)
{
  TCCR1B = 0;
}

void Timer1_start(unsigned int count) // ��������� ������ �� ��������� � �������������
{
  TCNT1 = 0;           // �������� ������� ��������
  OCR1A = count*2;
  TCCR1B = (0<<WGM12)|(0<<WGM13) |(0<<CS12)|(1<<CS11)|(0<<CS10) ; // ��������� ������ fclk/8
}

/********************************************************************/
/********************************************************************/
/********************************************************************/
unsigned int ButtonRead(void)     // ���������� ������, ���������� ����� ������� ������
{
unsigned int time_button = 0;
if(pressing)
     {
        if(PIN_Button &(1<<Button)) {if(timer_count_ms_read()<500){ time_button = timer_count_ms_read();pressing=0;}else{pressing=0;}}
        else {if(pressing!=2){if(timer_count_ms_read()>510){time_button = timer_count_ms_read();pressing=2;} }}
     }
        else  
     {
        if((~PIN_Button)&(1<<Button))  {timer_count_ms_write(0);pressing=1;}
     }
return time_button;
}

void usart_init (void)
{
 //��������� UART
UBRR0L = LO(bauddivider);
UBRR0H = HI(bauddivider);
UCSR0A = 0;
UCSR0B = 1<<RXEN0|1<<TXEN0|0<<RXCIE0|0<<TXCIE0|0<<UDRIE0;//��������� �����, �������� ������ � ���������� �� ����� �����
UCSR0C = 0b10000110; //8 ���, 1 ���� ���
}
 
 void WriteByteUART(unsigned char k)//  �������� �����
{
  while( ( UCSR0A & ( 1 << UDRE0 ) ) == 0  );
  UDR0 = k;
}

/*���������� � ���������� ������� � ���������� � UART*/
void WriteDecUART_6(unsigned int func)
{  // �������� �������������� �����
unsigned char k = 0;
if (func>9999) func=9999;
unsigned int n=10000;
for (unsigned char i=0; i < 4; i++){
 n=n/10;
unsigned int tetra= func/n;
 if (tetra>0)  k = 1;
unsigned char bukva=tetra+0x30; // ��������� ����� � ���
if (k>0|| i==3) WriteByteUART(bukva);
 func=func-tetra*n;
  }
}


//******************** ���������� ���� *****************************/
/*******************************************************************/
/*******************************************************************/
void MenyShow (void)
{
 if(PWM_on)LCDstringXY("on ",13,1); else LCDstringXY("off",13,1);

/*���������� ������� � �������*/
unsigned int Button_time = ButtonRead();
  if(Button_time)                         // ���� ������ �� ������, ��� ���������� ����, �� ������ �� ������
  {
  if(Button_time<500) { PWM_on ^= 1;  } // ���� ������ ������ ������ 500ms, �� ���/���� ���
  if(Button_time>500) // ���� ������ ������ ������ 1 �������, �� ������ ���������� ��� ����������� ���������
      {
       meny ^=1;  
       if(meny)EncoderWrite(PWM); else EncoderWrite(Angle); // ���������� �������� ���������� � �������
      } 
  }
/*������� ������� ������*/
LCDGotoXY(12,0);
BCD_4Int(Thrt);
LCDsendString(pBuf);

if(meny)
{
 PWM = EncoderRead();
 LCDstringXY("����: ",0,0);
 LCDstringXY("��� :-",0,1);
BCD_4Int(PWM);
LCDsendString(pBuf);
}
else
{
 Angle = EncoderRead();
 LCDstringXY("��� : ",0,1);
 LCDstringXY("����:-",0,0);
 BCD_4Int(Angle);
 LCDsendString(pBuf);
}
    // if(PIND&(1<<2)){SET(PORTB, 5);} else {CLR(PORTB, 5);}  // ��������� ������� ����������� �� ������� ����� ������ 
}

/****************************** main *******************************/
/*******************************************************************/
/*******************************************************************/
int main(void)
{ 
  _delay_ms(200);
  LCDinit();
  EncoderInit();
   LCDclear();
   ADC_Init();
   timer0_init();  // ����������� ������� ��� ��������
   Timer1_init();
   usart_init();//������������� ������

   Timer2_init();
   //��������� �������� ���������� �� INT0 (������ �����)
  EICRA = 0b00000011; //����������� ������� ����������(1 0��������� �����,1 1�����������)
  EIMSK = 0b00000001; //��������� ������� ���������� �� int0  

  CLR(DDRC, 0);   // ��� ADC0 �� ����
  DDRB |=(1<<5);  // ��������� �� �������
  DDRD |=(0<<2);  // ���� � ���� �������

  DDR_Button  |= (0<<Button); // ����� ������ �� ����
  PORT_Button |= (1<<Button); // ���������� ������������� ��������
  DDR_PWM |= (1<<PIN_PWM);    // ������������ ����� ���

  pBuf=BCD_GetPointerBuf();//������������ ���������� pBuf 
  //��������=========================
  LCDstringXY("������",5,0);  
  //LCDstringXY("������������ ���",0,1); 
  //BCD_3Int(67);
  //LCDsendString(pBuf);
//_delay_ms(1000);
LCDclear();

/*��������� ���������� ����*/
/***************************/
LCDstringXY("��� : ",0,1);
BCD_4Int(PWM);
LCDsendString(pBuf);

LCDstringXY("����: ",0,0);
//BCD_4Int(ADC_read(0));
BCD_4Int(Angle);
LCDsendString(pBuf);


while(1)
{

MenyShow();

/*************************************************************************/
 if (redyData)
 {
  redyData =0;
  SET(PORTB, 5); 
        // ���������� ��� ��� ���� � UART*/
        for (int t=1; t < MAX_SAMPLES; t++)
        {
           unsigned char val = zero[t];               // ��� ���������� �������� ��������� ��� ����
           zero[t]=0;
           unsigned int x = adc[t];

        WriteDecUART_6(x);
        WriteByteUART(0x09);             // "\t" - TAB
        if(val){WriteByteUART(0x31);} else {WriteByteUART(0x30);} // ���� ������=1 �� ���������� � ���� 1, ����� 0
        WriteByteUART(0x0D);             // CR  ������� ������
        WriteByteUART(0x0A);             // LF  ����� ������*
      //  MenyShow();

   /**********���������� ������� � �������*********/
   unsigned int Button_time = ButtonRead();
  if(Button_time)                         // ���� ������ �� ������, ��� ���������� ����, �� ������ �� ������
  {
  if(Button_time<500) { PWM_on ^= 1;  } // ���� ������ ������ ������ 500ms, �� ���/���� ���
  if(Button_time>500) // ���� ������ ������ ������ 1 �������, �� ������ ���������� ��� ����������� ���������
      {
       meny ^=1;  
       if(meny)EncoderWrite(PWM); else EncoderWrite(Angle); // ���������� �������� ���������� � �������
      } 
  }
  //**************************************************
        }
        WriteByteUART('E');
        WriteByteUART('n');
        WriteByteUART('d');
       CLR(PORTB, 5);
  }

/*************************************************************************/

}}
    






