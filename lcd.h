
/* 
Обязательно подключить вывод RW ! 
*/

#include <inttypes.h>

//---------------------------------------------------------------------------------------------
//≈сли  хочешь использовать 8ми битную схему подключени€, тогда раскомментируй #define LCD_8BIT
//#define LCD_8BIT
//---------------------------------------------------------------------------------------------

#ifndef LCD
#define LCD

#if 1


//”казываем порт к которому подключены выводы диспле€ LCD DB0...DB7.
#define DPIN  PINC
#define DDDR  DDRC
#define DPORT PORTC

//цифрами указываем номера выводов мк подключенные к дисплею.						
//ѕины      ћ   LCD   
#define DB0	6// DB0
#define DB1	6// DB1
#define DB2	6// DB2	
#define DB3	6// DB3		
#define DB4	2// DB4  
#define DB5	3// DB5
#define DB6	4// DB6
#define DB7	5// DB7 + BF флаг зан€тости диспле€.

//”казываем порт к которому подключены выводы диспле€ E, RS, R/W.
#define CDDR  DDRB
#define CPORT PORTB

// ”казываем номера пинов ћ , к которым подключаем дисплей.


#define E	4	// E	 —“–ќЅ.
#define RS	3 	// RS	 RS=0 посылаем команду в LCD, RS=1 посылаем данные в LCD.
#define RW	2   // R/W   R/W=1 читаем из LCD, R/W=0 записываем в LCD.

#endif
//----------------------------------Ќастройки закончены---------------------------------
#if 1
//ѕользовательские функции, ими пользуемс€ в программе.
                   
void LCDGotoXY(uint8_t,uint8_t);			             //”станавливаем курсор в X, Y позицию
void LCDdata(uint8_t);						             //¬ывести 1 символ на дисплей.
void LCDdataXY(uint8_t,uint8_t,uint8_t);	             //¬ывести 1 символ на дисплей в X, Y позицию .
void LCDsendString(char*);                             //¬ывести строку на дисплей
void LCDstringXY(char*,uint8_t,uint8_t);		             //¬ывести строку на дисплей в позицию x,y
void LCDstring_of_sramXY(uint8_t*,uint8_t,uint8_t);			 //¬ывести строку на дисплей в позицию x,y из ќ«”		
void LCDstring_of_flashXY(const uint8_t*,uint8_t, uint8_t);//¬ывести строку в позицию x,y из флеша

void LCDinit(void);							//»нициализаци€ LCD  
void LCDblank(void);			//—делать невидимым инфо на дисплее
void LCDnblank(void);			//—делать видимой инфо на дисплее + отключение видимых курсоров.
void LCDclear(void);			//ќчистить дисплей от инфо + курсор на позицию 0,0
void LCDcursor_bl(void);		//¬ключить мигающий курсор
void LCDcursor_on(void);		//¬ключить подчеркивающий курсор
void LCDcursor_vi(void);		//¬ключить оба курсора
void LCDcursorOFF(void);		//¬ыключить курсор (любой)
void LCDacr(void);				//Cчетчик адреса AC всегда будет смещатьс€ на n+1
void LCDacl(void);				//Cчетчик адреса AC всегда будет смещатьс€ на n-1
void LCDcursorl(void);			//—местить курсор влево на 1 символ
void LCDcursorr(void);			//—местить курсор вправо на 1 символ
void LCDcursorln(uint8_t);		//—местить курсор влево на n символов
void LCDcursorrn(uint8_t);		//—местить курсор вправо на n символов
void LCDscreenl(void);			//—местить экран влево на 1 символ
void LCDscreenr(void);			//—местить экран вправо на 1 символ
void LCDscreenln(uint8_t);		//—местить экран влево на n символов
void LCDscreenrn(uint8_t);		//—местить экран вправо на n символов
void LCDscreenL(void);			//— каждым новым символом экран будет смещатьс€ влево
void LCDscreenR(void);			//— каждым новым символом экран будет смещатьс€ вправо
void LCDresshift(void);			// урсор в позицию 0,0 + сброс всех сдвигов, изображение остаетс€


//4-x строчный дисплей.
#define LINE0 0x00
#define LINE1 0x40
#define LINE2 0x14
#define LINE3 0x54

#endif

/*Ѕ»ЅЋ»ќ“≈ ј ƒЋя ѕ≈–≈¬ќƒј ƒ¬ќ»„Ќџ’ „»—≈Ћ ¬ —»ћ¬ќЋџ » ¬џ¬ќƒј »’ Ќј ∆ ƒ*/
#if 1
/*здесь переопредел€ем функцию по выводу символа на экран или в терминал
если вывод не используетс€ оставл€ем пустой макрос */

#define BCD_SendData(data) //LCD_WriteData(data)

/*отображать ноль в старших разр€дах или нет. если закомментировать, то
ноль не будет отображатьс€*/

#define MIRROR_NULL

/*использовать буфер или нет. если закомментировать, то запись в буфер
не будет производитьс€ */

#define BCD_USE_BUF

/*переводить цифры в символы или оставл€ть в двоично-дес€тичном виде. если
закомментировать, то цифры будут сохран€тьс€ в двоично-дес€тичном виде, если
оставить то в символьном виде с завершающим нулевым символом*/

#define BCD_SYM

/*************************** пользовательские функции ***********************/

/*¬з€ть указатель на буфер. ≈сли буфер не используетс€,
будет возвращено нулевое значение */

uint8_t* BCD_GetPointerBuf(void);

/*ѕреобразование 8-ми разр€дных двоичных чисел. ќптимизированные функции дл€ 1, 2 и 3-ех
разр€дных дес€тичных чисел. ≈сли функции BCD_1(uint8_t value) передать 2 или 3-ех
разр€дное дес€тичное число, она будет работать некорректно. “о же самое относитс€
и к двум другим функци€м*/

void BCD_1(uint8_t value);
void BCD_2(uint8_t value);
void BCD_3(uint8_t value);

/*ѕреобразование 16-ти разр€дных двоичных чисел. ќптимизированные функции дл€ 3, 4 и 5-и
разр€дных дес€тичных чисел. ≈сли функции BCD_3Int(uint16_t value) передать 4 или 5-и
разр€дное дес€тичное число, она будет работать некорректно. “о же самое относитс€
и к двум другим функци€м*/

void BCD_3Int(uint16_t value);
void BCD_4Int(uint16_t value);
void BCD_5Int(uint16_t value);

/*‘ункции дл€ преобразовани€ 8, 16 и 32 разр€дных двоичных чисел. ¬се функции
корректно работают со своими типами данных. */

void BCD_Uchar(uint8_t value);
void BCD_Uint(uint16_t value);
void BCD_Ulong(uint32_t value);


#endif

#endif

//#define BCD_SendData(data) LCD_WriteData(data)

//Ќастройка конфигурации
/*
Ќастройка конфигурации включает в себ€ следующие шаги.

1. ѕодключение внешней библиотеки дл€ вывода данных. 
≈сли этот функционал не используетс€, эту строчку можно закомментировать.

//подключаю библиотеку дл€ вывода на lcd
#include "lcd_lib.h"

2. ѕереопределение функции вывода. ≈сли этот функционал не используетс€ макрос нужно оставить пустым.

//вывод не используетс€ оставл€ем макрос пустым
#define BCD_SendData(data)

//вывод используетс€, переопредел€ем функцию
#define BCD_SendData(data) LCD_WriteData(data)

3. ќтображение нул€ в старших разр€дах. ≈сли закомментировать настройку ноль выводитьс€ не будет, 
вместо него будет сохран€тьс€ символ пробела. ≈сли оставить, ноль будет отображатьс€.

//при такой настройке BCD_Uchar(3) отобразит на экране 003
#define MIRROR_NULL

//если закомментировать BCD_Uchar(3) отобразит на экране 3
//#define MIRROR_NULL

4. »спользование буфера.
¬ зависимости от настроек библиотеки, функции преобразовани€ чисел могут сохран€ть результат в буфере 
в виде строки, которую потом можно передать какой-нибудь функции вывода. ≈сли эту настройку 
закомментировать, то запись в буфер производитьс€ не будет.

#define BCD_BUF_USE

5. “ребуемый конечный результат. Ѕиблиотека позвол€ет переводить двоичные числа в двоично-дес€тичный вид 
или в символьный. ≈сли данна€ настройка закомментирована используетс€ двоично-дес€тичное представление, если нет, то символьное.

#define BCD_SYM

ќписание функций

ќбщие функции дл€ преобразовани€ 8, 16 и 32 разр€дных двоичных чисел.

void BCD_Uchar(uint8_t value) - преобразует числа от 0 до 255
void BCD_Uint(uint16_t value) - преобразует числа от 0 до 65535
void BCD_Ulong(uint32_t value) - преобразует числа от 0 до 4294967295

ѕример

#define F_CPU 9600000UL
...
uint8_t count = 120;
uint16_t adc_value = 1020;
...
BCD_Uchar(count);
BCD_Uint(adc_value);
BCD_Ulong(F_CPU);

‘ункции дл€ преобразовани€ дес€тичных чисел заданной разр€дности.

void BCD_1(uint8_t value) - преобразует числа от 0 до 9
void BCD_2(uint8_t value) - преобразует числа от 0 до 99
void BCD_3(uint8_t value) - преобразует числа от 0 до 255
void BCD_3Int(uint16_t value) - преобразует числа от 0 до 999
void BCD_4Int(uint16_t value) - преобразует числа от 0 до 9999
void BCD_5Int(uint16_t value) - преобразует числа от 0 до 65535

ѕример

#define DATA 23
...
//переменна€ котора€ всегда < 10
uint8_t index = 0;

//счетчик до 999
uint8_t count;
...
BCD_1(index);
BCD_2(DATA);
BCD_3Int(count);

//а это неправильно
BCD_1(DATA);
BCD_3(count);

‘ункци€ дл€ получени€ указател€ на буфер, в котором сохран€етс€ результат. ≈сли буфер не используетс€, функци€ возвращает нулевой указатель.

uint8_t* BCD_GetPointerBuf(void);

ѕример
//определ€ем переменную указатель на буфер
uint8_t* pBuf;

//инициализируем эту переменную
pBuf = BCD_GetPointerBuf();

//преобразуем число, а затем выводим строку на экран
BCD_3Int(counter);
LCD_SendStr(pBuf);*/
//ѕримеры использовани€ команд
/*
LCDcommand(0b00101000);//¬ключаем 4х битный интерфейс приема/передачи и выбираем 5x8 точек.
LCDcommand(0b00000001);//ќчистка экрана
LCDcommand(0b00000010);//”становка курсора в позицию 0,0 + сброс сдвигов
LCDcommand(0b00000110);//¬кл. инкримент счетчика адреса, движение изображени€ отк.
LCDcommand(0b00001100);//¬ключаем отображение на дисплее + без отображени€ курсора.

LCDdata('A');			//¬ывели символ ј
LCDGotoXY(6,1);			//ѕеревели курсор в седьмую позицию втора€ строка.
LCDdata('B');			//» вывели символ ¬
LCDdataXY('ў',0,0); ¬ывели символ ў в позицию 0,0
LCDdata('у');
LCDdata('к');
LCDdata('а');
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
LCDGotoXY(1,1);			    //ѕеревели курсор в позицию 1,1.
LCDsendString("ѕучеглазка");//¬ывели  ѕучеглазка
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
LCDstring(" расава ѕучеглазка",0,1);//¬ывели строку в позицию 0,1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
—перва объ€вим в озу uint8_t  text_1[]="ѕучеглазка_3";
LCDstring_of_sram(text_1,0,0); а теперь выведем строку в позицию 0,0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
—перва объ€вим во флеш const uint8_t PROGMEM text_1[]="ѕучеглазка_1";
LCDstring_of_flash(text_1,0,0); а теперь выведем строку в позицию 0,0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~							 						 
*/

/*
»нициализаци€ диспле€ проводитс€ таким образом.
1.¬ключаем питание.
2.ѕосле VCC >=4.5V ждем не менее 15 мсек.
3.ќтправка команды 0x30 - 0b110000
4.∆дем не менее 4.1 мсек
5.ќтправка команды 0x30
6.∆дем не менее 100 мксек
7.ќтправка команды 0x30
ѕриведенные выше операции €вл€ютс€ инициализирующими дл€ LCD
и способны вывести дисплей в рабочее состо€ние из любого состо€ни€.
8.ƒалее работаем с дисплеем в обычном режиме.

¬ исходном состо€нии ≈=0, R/W=0, RS - произвольное, DB0...DB7 высокий импеданс (HI).
“акое состо€ние должно сигналов ≈=0, R/W=0 должно поддерживатьс€ все врем€ в пррмежутках между
операци€ми обмена данными с ћ .

ƒисплей настраиваем так:

int main(void)
{
	init();		//»нициализаци€ ћ .
	LCDinit();	//»нициализаци€ LCD, эту функцию в первую очередь, потом все остальное.

	while(1)//√лавный цикл программы.
	{
		
	}
}

 арта символов диспле€.
LCDGotoXY(3,1);
÷ифра 3 означает четвертую позицию в любой строке. ≈сли 0, то это перва€ позици€ в любой строке.
цифра 1 означает нижнюю строку а 0 верхнюю.
|0,0|1,0|2,0|3,0|4,0|5,0|6,0|7,0| - перва€ строка
|0,1|1,1|2,1|3,1|4,1|5,1|6,1|7,1| - втора€ строка

1  0  0
1 I/D S  
I/D - смещение счетчика адреса, 0-уменьшение 1-увеличение
S   - сдвиг содержимого экрана 0 содержимое не сдвигаетс€, 1 сдвигаетс€, 
      если I/D - 0 то вправо, если 1 то влево.
LCDcommand(100) - счетчик n-1, экран не сдвигаетс€. —имволы будут выводитс€  <-
LCDcommand(110) - счетчик n+1, экран не сдвигаетс€. —имволы будут выводитс€  ->
LCDcommand(101)	- счетчик n-1, изображение сдвигаетс€ вправо с каждым новым символом
LCDcommand(111) - счетчик n+1, изображение сдвигаетс€ влево с каждым новым символом
	   	   
1   0   0   0  0	   
1  S/C R/L  -  -     
S/C (screen/cursor) - 0 сдвигаетс€ курсор, 1 сдвигаетс€ экран.
R/L (right/left)    - 0 сдвиг влево, 1 сдвиг вправо. за одну команду на 1 сдвиг
*/